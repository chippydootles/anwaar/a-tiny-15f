Anwaar are families of standardized boards for making your own personal pcb token. 
The A-TINY-15F is the first base board of the Anwaar Amulet family.

More info can be found: https://chippydootles.com/anwaar/amulet/

The A-TINY-15F is certified with OSHWA:
[OSHW] US002580 | Certified open source hardware | oshwa.org/cert

About this repository:
3D - 3d Exports of board
Drawings - Folder for SVG or DXF exports
License - License information
PDF - PDF exports of schmatic
Production Files - Files for fabrication

changes.txt - As per CERN OHL V2 changes made to this must be listed with a times stamp in this file
