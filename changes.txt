Version 2.0 - By Chipperdoodles 3/15/23 2023
 * Board redesigned to USB-C

Version 2.1 - by Chipperdoodles 5/1/2023
 * LDO charging circuit replaced with dedicated charging IC

Version 3.0 - By Chipperdoodles
 * Boost converter replaced with TPS63900 Buckboost
 * UDPI pin changed CFG1 pin
 * UDPI test point added
 * layout changed to match older versions polarity
 * masked over pads added to back board

Version 4.0 - By Chipperdoodles 10/4/2023
 * Ch340e replaced with CP2105 (WCH342F possible pin compatible)
 * Serial debugging in addition to serial-udpi added with new uart chip
 * Enlarged Test Pads
 * Resistors for ROHM Ic changed to 0603 for easier visual inspection
 * Paste Layer updated on SMD standoffs. Enlarged.

Version 4.1 - By Chipperdoodles 12/14/2023
 * Minor revision
 * CP2105 (U1) replaced by WCH342F
 * 1 Capacitor (C3) removed
 * version silk and copper marking updated
 * signature changed, traces adjusted, footprints locked